//
//  Film.swift
//  Че глянуть?
//
//  Created by Александра on 04.06.2021.
//

import Foundation

struct FilmData: Decodable {
    let movies: [Film]
    
    private enum CodingKeys: String, CodingKey {
        case movies = "results"
    }
}

struct Film: Decodable {
    let adult: Bool?
    let id: Int?
    let title: String?
    let year: String?
    let rate: Double?
    let posterImage: String?
    let overview: String?
    let genres: [Int]?
    
    private enum CodingKeys: String, CodingKey {
        case title, overview, adult
        case id
        case genres = "genre_ids"
        case year = "release_date"
        case rate = "vote_average"
        case posterImage = "poster_path"
    }

}
