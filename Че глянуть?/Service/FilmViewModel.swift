//
//  FilmViewModel.swift
//  Че глянуть?
//
//  Created by Александра on 06.06.2021.
//

import Foundation

class FilmViewModel {
    var popularMovie: Film?
    
    private var apiService = ApiService()
    
    init() { }
    
    
    func fetchPopularMoviesData(completion: @escaping () -> ()) {
        
        // weak self - prevent retain cycles
        apiService.getPopularMoviesData { [weak self] (result) in
            
            switch result {
            case .success(let movie):
                self?.popularMovie = movie
                completion()
            case .failure(let error):
                // Something is wrong with the JSON file or the model
                print("Error processing json data: \(error)")
            }
        }
    }

}
